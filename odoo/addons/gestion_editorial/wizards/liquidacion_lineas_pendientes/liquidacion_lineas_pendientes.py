from odoo import api, fields, models, exceptions

class LiquidacionWizard(models.TransientModel):
    """ Wizard: Ayuda para seleccionar las stock.move.lines pendientes de liquidar """
    _name = 'liquidacion.wizard.descontrol'
    _description =  "Wizard Liquidación"

    partner_id = fields.Many2one('res.partner', string='Cliente')

    liquidacion_line_ids = fields.One2many('liquidacion.line.descontrol', 'liquidacion_wizard_id', string="Lineas de Liquidacion", copy=True)

    @api.onchange('partner_id')
    def _update_invoice_lines(self):
        for wizard in self:
            domain = [
                ('owner_id', '=', self.env.context.get('partner_id')),
                ('location_id', '=' , 17), # This is the id of the Location DS/Depósitos (the source of the move)
                ('location_dest_id', '=' , 5), # This is the id of the Location Partner Locations/Customers (the destination of the move)
                ('state', 'in', ('assigned', 'partially_available')),
            ]
            pendientes_liquidar_line_ids = self.env['stock.move.line'].search(domain)
            wizard.liquidacion_line_ids = None
            for move_line in pendientes_liquidar_line_ids:
                wizard._update_liquidacion_line(move_line)

    def _update_liquidacion_line(self, move_line):
        liquidacion_line_aux = self.liquidacion_line_ids.filtered(lambda line: line.product_id == move_line.product_id)
        if len(liquidacion_line_aux) <= 0:
            liquidacion_line = self.env['liquidacion.line.descontrol'].create({'liquidacion_wizard_id': self.id, 'product_id': move_line.product_id.id})
        else:
            liquidacion_line = liquidacion_line_aux[0]
        total_product_uom_qty = liquidacion_line.total_product_uom_qty + move_line.product_uom_qty
        liquidacion_line.update({'total_product_uom_qty': total_product_uom_qty})
        liquidacion_line.total_qty_done += move_line.qty_done
        return liquidacion_line

    @api.onchange('liquidacion_line_ids')
    def _check_liquidacion_lines(self):
        for wizard in self:
            for liquidacion_line in wizard.liquidacion_line_ids:
                if liquidacion_line.total_qty_a_liquidar and liquidacion_line.total_qty_a_liquidar > 0.0:
                    value = liquidacion_line.total_product_uom_qty - liquidacion_line.total_qty_done
                    if liquidacion_line.total_qty_a_liquidar > liquidacion_line.total_product_uom_qty - liquidacion_line.total_qty_done:
                        raise exceptions.ValidationError("La cantidad a liquidar no puede ser mayor que la diferencia entre el total en depósito y el total hecho.")

    @api.model
    def default_get(self, fields):
        res = super(LiquidacionWizard, self).default_get(fields)
        res['partner_id'] = self.env.context.get('partner_id')
        return res

    def seleccionar_para_liquidar(self):
        # self.ensure_one()
        liquidacion = self.env['account.move'].browse(self.env.context.get('liquidacion_id'))
        for liquidacion_line in self.liquidacion_line_ids:
            if liquidacion_line.total_qty_a_liquidar > 0.0:
                # TODO esto sólo funcionaría para las tarifas con una línea (item_ids[0]):
                price_unit = liquidacion_line.product_id.list_price * (1 - (liquidacion.pricelist_id.item_ids[0].percent_price or 0.0) / 100.0)
                quantity = liquidacion_line.total_qty_a_liquidar
                product = liquidacion_line.product_id
                partner = self.partner_id.id
                vals = {
                    'name': liquidacion_line.product_id.name,
                    'move_id': self.env.context.get('liquidacion_id'),
                    'partner_id': partner,
                    'product_id': product.id,
                    'journal_id': liquidacion.journal_id,
                    'quantity': quantity,
                    'price_unit': price_unit,
                    'tax_ids': product.taxes_id,
                }
                liquidacion.write({'invoice_line_ids': [(0,0,vals)]}) # = self.env['account.move.line'].new(vals)
        liquidacion._move_autocomplete_invoice_lines_values()
        liquidacion._recompute_payment_terms_lines()
        return {'type': 'ir.actions.act_window_close'}

    def seleccionar_para_devolver(self):
        domain = [
            ('owner_id', '=', self.partner_id.id),
            ('location_id', '=' , 17), # This is the id of the Location DS/Depósitos (the source of the move)
            ('location_dest_id', '=' , 5), # This is the id of the Location Partner Locations/Customers (the destination of the move)
            ('state', 'in', ('assigned', 'partially_available')),
        ]
        pendientes_liquidar_line_ids = self.env['stock.move.line'].search(domain, order='date asc') # deposito de este contacto
        pickings_to_assign = set()
        pickings_return = {} # relation between done_picking (key) and the created_return (value)
        for liquidacion_line in self.liquidacion_line_ids:
            if liquidacion_line.total_qty_a_liquidar > 0.0:
                liquidacion_line_qty = liquidacion_line.total_qty_a_liquidar
                pendientes_cerrar_stock_lines = pendientes_liquidar_line_ids.filtered(lambda pendientes_liquidar_line: pendientes_liquidar_line.product_id == liquidacion_line.product_id)

                if not pendientes_cerrar_stock_lines or len(pendientes_cerrar_stock_lines) <= 0:
                    raise exceptions.ValidationError("No hay stock suficiente disponible en depósito para devolver. Intenta volver a comprobar el depósito")
                for pendiente_stock_line in pendientes_cerrar_stock_lines:
                    if liquidacion_line_qty > 0:
                        stock_line_reserved_qty = pendiente_stock_line.product_uom_qty
                        stock_line_done_qty = pendiente_stock_line.qty_done
                        qty_deposito = stock_line_reserved_qty - stock_line_done_qty
                        qty_difference = liquidacion_line_qty - qty_deposito
                        # if the liquidacion_qty is greater (or equal) than the available qty_deposito,
                        # we look for the associated stock.picking that enabled the current move.line de depósito,
                        # and then we return an amount of qty_deposito of the product_id on the associated stock.picking
                        
                        pickings_to_assign.add(pendiente_stock_line.picking_id) # associated deposito picking
                        
                        associated_done_picking = pendiente_stock_line.picking_id.sale_id.picking_ids.filtered(lambda picking: picking.location_id.id == 8
                                                                                                                               and picking.location_dest_id.id == 17
                                                                                                                               and picking.state == 'done'
                                                                                                                               and liquidacion_line.product_id.id in [l.product_id.id for l in picking.move_line_ids_without_package]
                                                                                                              )
                        if len(associated_done_picking) > 1:
                            associated_done_picking = associated_done_picking[1]
                        # Check if we have already created a return for this done_picking
                        if associated_done_picking not in pickings_return:
                            # New Wizard to make the return of one line
                            return_picking = self.env['stock.return.picking'].create({'picking_id': associated_done_picking.id})
                            pickings_return[associated_done_picking] = return_picking
                            return_picking._onchange_picking_id() # what does this do?
                        return_picking = pickings_return.get(associated_done_picking)
                        # Correct the lines with negative prohibited values
                        for line in return_picking.product_return_moves:
                            if line.quantity < 0:
                                line.write({'quantity': 0})

                        return_picking_line = return_picking.product_return_moves.filtered(lambda line: line.product_id == liquidacion_line.product_id)
                        return_qty = qty_deposito if qty_difference >= 0 else liquidacion_line_qty
                        get_qty = return_picking_line.quantity
                        return_picking_line.write({'quantity': get_qty + return_qty}) # we add to the existing value
                        liquidacion_line_qty = qty_difference
                    else:
                        break
        for return_picking in pickings_return.values():
            new_stock_picking_data = return_picking.create_returns()
            new_stock_picking = self.env['stock.picking'].browse(new_stock_picking_data['res_id'])
            for line in new_stock_picking.move_ids_without_package:
                line.write({'quantity_done': line.product_uom_qty})
            new_stock_picking.button_validate()
        for picking in pickings_to_assign:
            # Check availavility to load again the stock.move.lines after the return
            picking.action_assign()
        return {'type': 'ir.actions.act_window_close'}


class EditorialLiquidacionLine(models.TransientModel):
    """ Modelo de línea de liquidación"""
    _name = "liquidacion.line.descontrol"
    _description = "Linea Liquidacion Descontrol"

    # company_id = fields.Many2one(related='liquidacion_id.company_id', store=True, readonly=True)
    liquidacion_wizard_id = fields.Many2one('liquidacion.wizard.descontrol', "Liquidacion Wizard", index=True, ondelete="cascade")
    product_id = fields.Many2one('product.product', 'Producto')
    product_barcode = fields.Char('Código de barras / ISBN', related='product_id.barcode', store=True, readonly=True)
    product_name = fields.Char('Nombre', related='product_id.name', store=True, readonly=True)
    total_product_uom_qty = fields.Float('Total en Depósito', default=0.0, digits='Product Unit of Measure', required=True, copy=False)
    total_qty_done = fields.Float('Total Hecho', default=0.0, digits='Product Unit of Measure', copy=False)
    total_qty_disponibles = fields.Float('Total en depósito', default=0.0, digits='Product Unit of Measure', copy=False, compute="_compute_available")
    total_qty_a_liquidar = fields.Float('A Liquidar', default=0.0, digits='Product Unit of Measure', copy=False)

    @api.depends('total_qty_done', 'total_product_uom_qty')
    def _compute_available(self):
        for record in self:
            record.total_qty_disponibles = record.total_product_uom_qty - record.total_qty_done
