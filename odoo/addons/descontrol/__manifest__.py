# -*- coding: utf-8 -*-
{
    'name': "descontrol",

    'summary': """
        Personalització per a Descontrol SCCL""",

    'description': """
        Mòdul de personalització amb dependències, configuracions i productes
    """,

    'author': "Devcontrol",
    'website': "http://localhost",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    #'category': 'Uncategorized',
    'category': 'Customization',
    'version': '13.0.0.0.8',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        #'views/views.xml',
        #'views/templates.xml',
        'data/company.xml',
        #'data/sales.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
}
